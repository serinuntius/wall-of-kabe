﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Threading;
using System;

public class ArduinoSerialIOScript : MonoBehaviour {
	int x1=0, y1=0, z1=0;
	int x2=0, y2=0, z2=0;

	int pre1=0,pre2=0;

	int px1=0, py1=0, pz1=0;
	int px2=0, py2=0, pz2=0;
	float move = 0;
	float move1 = 0;
	
	float move2 = 0;

	bool a =false;
	
	SerialPort stream1 = new SerialPort("/dev/tty.usbmodem1411", 9600);
	SerialPort stream2 = new SerialPort("/dev/tty.usbmodem1431", 9600);

	
	void Start () {
		OpenConnection ();
	}
	void Update () {
		
		string result1="";
		string result2="";

		
		
		result1 = stream1.ReadLine();
		result2 = stream2.ReadLine();
		//Debug.Log (result1);

		if (result1 != null) {
			string[] r = result1.Split (':');
			for (int i=1; i<r.Length; i++) {
				//Debug.Log("i:"+i+" r:"+r[i]);
				//Debug.Log(r[i].Substring(0,3));
				if(a){
					if(i==1){
						x1 = int.Parse(r[i].Substring(0,3)); 
					}
					if(i==2){
						y1 = int.Parse(r[i].Substring(0,3)); 
					}
					if(i==3){
						z1 = int.Parse(r[i].Substring(0,3)); 
					}

				}
				else{
					if(i==1){
						x2 = int.Parse(r[i].Substring(0,3)); 
					}
					if(i==2){
						y2 = int.Parse(r[i].Substring(0,3)); 
					}
					if(i==3){
						z2 = int.Parse(r[i].Substring(0,3)); 
					}



				}
				a =! a;


				/*
				if (r [0] == "X1") {
					x1 = int.Parse(r[1]);
					
				}
				else if (r [0] == "Y1") {
					y1 = int.Parse(r[1]);
					
				}
				else if (r [0] == "Z1") {
					z1 = int.Parse(r[1]);
					
				}
				else if (r [0] == "X2") {
					x2 = int.Parse(r[1]);
					
				}
				else if (r [0] == "Y2") {
					y2 = int.Parse(r[1]);
					
				}
				else if (r [0] == "Z2") {
					z2 = int.Parse(r[1]);
					
				}*/
				//Debug.Log ("X:"+x+"Y:"+y+"Z:"+z);
			}

		}

		if (result2 != null) {
			//Debug.Log (result2);
			string[] r2 = result2.Split (':');
			for (int i=0; i<r2.Length; i++) {
				//Debug.Log (r2[i]);
				if (r2 [0] == "R") {
					pre1 = int.Parse(r2[1]); 
					
				}
				else if (r2 [0] == "L") {
					pre2 = int.Parse(r2[1]); 
					
				}
			}

		}


		//transform.Rotate (px-x/10,py-y/10,pz-z/10);

		move1 = (float)Math.Pow(Math.Pow (px1 - x1, 2) + Math.Pow (py1 - y1, 2) + Math.Pow (pz1 - z1, 2)  , 0.5);
		move2 = (float)Math.Pow(Math.Pow (px2 - x2, 2) + Math.Pow (py2 - y2, 2) + Math.Pow (pz2 - z2, 2)  , 0.5);
		Debug.Log ("x1:"+x1+" y1:"+y1+" z1:"+z1);
		Debug.Log ("px1:"+px1+" py1:"+py1+" pz1:"+pz1);

		px1 = x1;
		py1 = y1;
		pz1 = z1;
		
		px2 = x2;
		py2 = y2;
		pz2 = z2;

		//Debug.Log ("move1: "+move1+" move2: "+move2);
		
		if(move1!=0 || move2!=0){
			if(move1>move2){
				move = move1;
			}
			else{
				move = move2;
			}
			
		}
		/*
		if(pre1<200 && pre2<200){
			move = -1;

		}
		*/
		transform.Translate (0,(float)0.00001*move1,0);
		transform.Translate (0,(float)0.00001*move2,0);


		//Debug.Log ("move1: "+move1);
		//Debug.Log ("move2: "+move2);

		

		//Debug.Log ("pre1: "+pre1);
		//Debug.Log ("pre2: "+pre2);
		//Debug.Log ("move1: "+move1);
		//Debug.Log ("move2: "+move2);

		//Debug.Log("move: "+move);
		move = 0;
	}
	
	void OnApplicationQuit(){
		stream1.Close();
		stream2.Close();

	}
	
	void OpenConnection() {
		
		if (stream1 != null) {
			
			if (stream1.IsOpen) {
				stream1.Close ();
				Debug.LogError ("Failed to open Serial Port, already open!");
			} else {
				stream1.Open ();
				stream1.ReadTimeout = 200;
				
				Debug.Log ("Open Serial port1");      
			}
		}if (stream2 != null) {
			
			if (stream2.IsOpen) {
				stream2.Close ();
				Debug.LogError ("Failed to open Serial Port, already open!");
			} else {
				stream2.Open ();
				stream2.ReadTimeout = 200;
				
				Debug.Log ("Open Serial port2");      
			}
		}
	}
}